﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreBoard : MonoBehaviour
{

    int score = 0;
    float timeLastScoring = 0f;
    Text textComponent;
    [Tooltip("Sec")] [SerializeField] float timeForDoubeScore = 1f;

    // Use this for initialization
    void Start()
    {
        textComponent = GetComponent<Text>();
        textComponent.text = score.ToString();
    }

    public void ScoreOnKill(int scorePerKill)
    {
        if (Time.time - timeLastScoring < timeForDoubeScore)
        {
            score = score + scorePerKill * 2;
            textComponent.text = score.ToString();
            timeLastScoring = Time.time;
        }
        else
        {
            score = score + scorePerKill;
            textComponent.text = score.ToString();
            timeLastScoring = Time.time;
        }
    }
    public void ScoreOnHit(int scorePerHit)
    {
        score = score + scorePerHit;
        textComponent.text = score.ToString();
    }
    // Update is called once per frame

}
