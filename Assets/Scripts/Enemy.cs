﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{


    [SerializeField] Transform parent;
    [Tooltip("")] [SerializeField] GameObject FXExplosion;
    [SerializeField] int scorePerHit = 1;
    [SerializeField] int scorePerKill = 100;
    [SerializeField] int hits = 10;

    Collider boxCollider;
    ScoreBoard scoreBoard;
    // Use this for initialization
    void Start()
    {
        AddBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void AddBoxCollider()
    {
        boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
    }

    private void OnParticleCollision(GameObject other)
    {
        ProcessHit();

        if (hits <= 0)
        {
            KillEnemy();
        }
    }

    private void ProcessHit()
    {
        scoreBoard.ScoreOnHit(scorePerHit);
        hits--;
    }

    private void KillEnemy()
    {
        GameObject explosion = Instantiate(FXExplosion, transform.position, Quaternion.identity);
        explosion.transform.parent = parent;

        scoreBoard.ScoreOnKill(scorePerKill);

        Destroy(gameObject);
    }
}
