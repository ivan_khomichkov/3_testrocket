﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{

    SceneLoader sceneLoader;
    [Header("Explosion Effects")]
    [Tooltip("FX for explosion.")][SerializeField] GameObject Explosion;
    [Tooltip("FX for ship falling apart.")] [SerializeField] GameObject ShipApart;
    
    private void OnTriggerEnter(Collider other)
    {
        TurnOnTheGravity();
        StartDeathSequence();
    }

    private void TurnOnTheGravity()
    {
        Rigidbody rigidbody = this.GetComponent<Rigidbody>();
        if (rigidbody!=null)
        {
            rigidbody.isKinematic = false; 
        }
    }

    private void StartDeathSequence()
    {

        Instantiate(Explosion,transform.position,Quaternion.identity);
        Instantiate(ShipApart, transform.position, Quaternion.identity);
        
        SendMessage("TurnOfTheControll"); // method is in PlayerController class
        RestartLevel();
        Destroy(gameObject);

    }

    void RestartLevel() // String refferenced
    {
        FindObjectOfType<SceneLoader>().LoadLevelWithDelay();
    }
   
   
}
