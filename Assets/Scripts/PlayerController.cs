﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
public class PlayerController : MonoBehaviour
{

    [Header("Controll-Factors")]
    [Tooltip("In ms^-1")] [SerializeField] float xSpeed = 20f;
    [Tooltip("In ms^-1")] [SerializeField] float ySpeed = 20f;
    [Tooltip("Pitch factor duaring yThrow.")] [SerializeField] float ControllPitchFactor = -5f;
    [Tooltip("Roll factor duaring xThrow.")] [SerializeField] float ControllRollFactor = -15f;
    [SerializeField] GameObject[] Guns;

    [Header("Screen-Positioning")]
    [Tooltip("Range that ship can from center to X axis.")] [SerializeField] float xRange = 4f;
    [Tooltip("Range that ship can from center to Y axis.")] [SerializeField] float yRange = 3f;
    [SerializeField] float PositionPitchFactor = -7f;
    [SerializeField] float PositionYawFactor = 7f;

    float xThroaw, yThroaw;
    private bool playerDead=false;


    // Update is called once per frame
    void Update()
    {
        if (!playerDead)
        {
            ProcessXThrow();
            ProcessYThrow();
            ProcessRotation();
            ProcessFire();
        }
    }

    public void TurnOfTheControll() //Called by string refference
    {
        playerDead = true;
    }
   
    private void ProcessRotation()
    {
        float pitchDueToPosition = transform.localPosition.y * PositionPitchFactor;
        float pitchDueToControllThrow = yThroaw * ControllPitchFactor;
        float Pinch = pitchDueToControllThrow + pitchDueToPosition; // высота (вращение вокруг x) - подьем + снижение
        float Yaw = transform.localPosition.x * PositionYawFactor;// поворот (вращение вокруг y) - лево + право
        float Roll = xThroaw * ControllRollFactor; // вращение (вращение вокруг z) - по часовой + против 
        transform.localRotation = Quaternion.Euler(Pinch, Yaw, Roll);
    }

    private void ProcessXThrow()
    {
        xThroaw = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThroaw * Time.deltaTime * xSpeed;
        float rawNewPos = Mathf.Clamp(transform.localPosition.x + xOffset, -xRange, xRange);
        transform.localPosition = new Vector3(rawNewPos, transform.localPosition.y, transform.localPosition.z);
    }
    private void ProcessYThrow()
    {
        yThroaw = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset = yThroaw * Time.deltaTime * ySpeed;
        float rawNewPos = Mathf.Clamp(transform.localPosition.y + yOffset, -yRange, yRange);
        transform.localPosition = new Vector3(transform.localPosition.x, rawNewPos, transform.localPosition.z);
    }

    private void ProcessFire()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            ActivateGuns(true);

        }
        else
        {
            ActivateGuns(false);
        }
    }

    private void ActivateGuns(bool activate)
    {
        foreach (var item in Guns)
        {
            //item.SetActive(activate);
            var ps = item.GetComponent<ParticleSystem>().emission ;
            ps.enabled = activate;
        }
    }
}
