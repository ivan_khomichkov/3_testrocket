﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    // Use this for initialization
    [SerializeField] float DelayInLoad;
    [SerializeField] bool LoadOnStart = false;

    private void Start()
    {
        if (LoadOnStart)
        {
            LoadLevelWithDelay();
        }
    }
    public void LoadLevelWithDelay()
    {
        Invoke("LoadLevel", DelayInLoad);
    }
        
    

    private void LoadLevel()
    {
        SceneManager.LoadScene(1);
    }

}
